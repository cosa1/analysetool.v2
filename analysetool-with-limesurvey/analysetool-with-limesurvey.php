<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              -
 * @since             1.0.1
 * @package           Analysetool_With_Limesurvey
 *
 * @wordpress-plugin
 * Plugin Name:       analysetool_with_limesurvey
 * Plugin URI:        -
 * Description:       Dieses Plugin verbindet wordpress mit Limesurvey
 * Version:           1.0.1
 * Author:            Markus Domin
 * Author URI:        -
 * Text Domain:       analysetool-with-limesurvey
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-analysetool-with-limesurvey-activator.php
 */
function activate_analysetool_with_limesurvey() {

	require_once plugin_dir_path( __FILE__ ) . 'includes/class-analysetool-with-limesurvey-activator.php';
	Analysetool_With_Limesurvey_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-analysetool-with-limesurvey-deactivator.php
 */
function deactivate_analysetool_with_limesurvey() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-analysetool-with-limesurvey-deactivator.php';
	Analysetool_With_Limesurvey_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_analysetool_with_limesurvey' );
register_deactivation_hook( __FILE__, 'deactivate_analysetool_with_limesurvey' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-analysetool-with-limesurvey.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_analysetool_with_limesurvey() {

	$plugin = new Analysetool_With_Limesurvey();
	$plugin->run();

}
run_analysetool_with_limesurvey();
