<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       -
 * @since      1.0.0
 *
 * @package    Analysetool_With_Limesurvey
 * @subpackage Analysetool_With_Limesurvey/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Analysetool_With_Limesurvey
 * @subpackage Analysetool_With_Limesurvey/public
 * @author     Markus Domin <markus.domin@fh-luebeck.de>
 */
class Analysetool_With_Limesurvey_Public
{

    /**
     * The ID of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $plugin_name    The ID of this plugin.
     */
    private $plugin_name;

    /**
     * The version of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $version    The current version of this plugin.
     */
    private $version;

    private $database;

    /**
     * Initialize the class and set its properties.
     *
     * @since    1.0.0
     * @param      string    $plugin_name       The name of the plugin.
     * @param      string    $version    The version of this plugin.
     */
    public function __construct($plugin_name, $version, $database)
    {

        $this->plugin_name = $plugin_name;
        $this->version = $version;
        $this->database = $database;

    }

    /**
     * Register the stylesheets for the public-facing side of the site.
     *
     * @since    1.0.0
     */
    public function enqueue_styles()
    {

        //wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/analysetool-with-limesurvey-public.css', array(), $this->version, 'all' );
        wp_enqueue_style($this->plugin_name, plugin_dir_url(__FILE__) . 'css/analysetool-with-limesurvey-public.less', array(), $this->version, 'all');

        //wp_enqueue_style( $this->plugin_name.'materialize', 'https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css' );
        wp_enqueue_style($this->plugin_name . "materialicons", 'https://fonts.googleapis.com/icon?family=Material+Icons');
        wp_enqueue_style($this->plugin_name . "fontawesome", 'https://use.fontawesome.com/releases/v5.4.1/css/all.css');

        //wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/analysetool-with-limesurvey-public.css', array(), $this->version, 'all' );
        //wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/analysetool-with-limesurvey-public.less', array(), $this->version, 'all' );

        //wp_register_style( 'jqueryuicss', 'https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css');
        //wp_enqueue_style('jqueryuicss');

        //wp_register_style( 'tabulatorcss', 'https://cdnjs.cloudflare.com/ajax/libs/tabulator/3.4.5/css/tabulator.css');
        //wp_enqueue_style('tabulatorcss');

        wp_enqueue_style($this->plugin_name . "toaster", 'https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css');
        //cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js
        //cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css

        wp_enqueue_style($this->plugin_name.'chardin', plugin_dir_url(__FILE__) . 'chardin/chardinjs.css', array(), $this->version, 'all');
        wp_enqueue_style($this->plugin_name.'enjoyhint', plugin_dir_url(__FILE__) . 'enjoyhint/enjoyhint.css', array(), $this->version, 'all');

    }

    /**
     * Register the JavaScript for the public-facing side of the site.
     *
     * @since    1.0.0
     */
    public function enqueue_scripts()
    {
        wp_enqueue_script($this->plugin_name . 'chardin', plugin_dir_url(__FILE__) . 'chardin/chardinjs.js', array('jquery'), '', true);
        wp_enqueue_script($this->plugin_name . 'enjoyhint', plugin_dir_url(__FILE__) . 'enjoyhint/enjoyhint.js', array('jquery'), '', true);
       
        wp_enqueue_script($this->plugin_name, plugin_dir_url(__FILE__) . 'js/analysetool-with-limesurvey-public.js', array('jquery',$this->plugin_name . 'chardin'), $this->version, false);
        // wp_register_script( 'jqueryui',"https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js",array('jquery'));
        // wp_enqueue_script('jqueryui');
        // wp_register_script( 'tabulator',"https://cdnjs.cloudflare.com/ajax/libs/tabulator/3.4.5/js/tabulator.js");
        // wp_enqueue_script('tabulator');
        //

        //wp_register_script( $this->plugin_name.'materialize','https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.js' );
        wp_enqueue_script($this->plugin_name . 'materialize', 'https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.js', array('jquery'), '', true);
        wp_register_script('qrcode', 'https://cdnjs.cloudflare.com/ajax/libs/jquery.qrcode/1.0/jquery.qrcode.min.js');
        wp_enqueue_script('qrcode');
        wp_register_script('less', 'https://cdnjs.cloudflare.com/ajax/libs/less.js/3.8.1/less.js');
        wp_enqueue_script('less');
        //wp_register_script('Chartjs', 'https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.min.js');
        wp_register_script('Chartjs', 'https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js');
        wp_enqueue_script('Chartjs');
        wp_register_script('Chartjsannotation', 'https://cdnjs.cloudflare.com/ajax/libs/chartjs-plugin-annotation/0.5.7/chartjs-plugin-annotation.min.js');
        wp_enqueue_script('Chartjsannotation');

        wp_enqueue_script($this->plugin_name . 'toaster', 'https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js', array('jquery'), '', true);

        
    }

    public function make_stylesheet_less($tag)
    {
        return preg_replace("/='stylesheet' id='analysetool-with-limesurvey-css'/", "='stylesheet/less' id='analysetool-with-limesurvey-css'", $tag);
    }

    public function anatool_frontend_create_company()
    {
        if (!is_user_logged_in()) {
            return null;
        }
        ob_start();
        $anatool_input_action = "anatoolsave";
        $anatool_input_field = "anatoolcompany";

        if (!isset($_POST[$anatool_input_field]) || !isset($_POST['companyname'])
            || !wp_verify_nonce($_POST[$anatool_input_field], $anatool_input_action)) {
            //print 'keine daten';
        } else {
            //print 'save data';
            //var_dump($_POST);
            $companyname = sanitize_text_field($_POST['companyname']);
            if ($companyname != "") {
                $this->database->anatool_db_insert_frima($companyname);
            }

            //$_POST['companyname']="";
            //echo '<script>window.location.reload();</script>';
        }

        ?>
	  <form name="anatool_input" action="" method="POST">
	    <?php wp_nonce_field($anatool_input_action, $anatool_input_field);?>
	    <fieldset>
	        <div>
	            <label for="companyname">Name des Betriebs</label><br>
	            <input type="text" id="companyname" name="companyname"/>
	        </div>
	    </fieldset><br>
	    <div><input type="submit" value="Hinzufügen" name="submit_addcompany_btn"></div>
	  </form>
	  <?php

        return ob_get_clean();
    }

    public function anatool_frontend_show_companys($atts)
    {
        if (!is_user_logged_in()) {
            return null;
        }
        ob_start();
        $selectcompanyid = self::getsysparam('selectcompanyid');
        if ($selectcompanyid != null) {
            $company = $this->database->anatool_db_check_companyid($selectcompanyid[0]);
            if ($company != null) {

            }
        } else {
            $anatool_input_action = "anatoolselect";
            $anatool_input_field = "anatoolcompanyselect";

            if (!isset($_POST[$anatool_input_field]) || !isset($_POST['companyid'])
                || !wp_verify_nonce($_POST[$anatool_input_field], $anatool_input_action)) {
                $companys = $this->database->anatool_db_get_all_companys();
                $adminsettings = get_option('anatool_option_name');
                $pageid = $adminsettings['companytokenlist'];
                $pageurl = get_permalink($pageid);
                ?>
	      <div style=" display: inline-block;
	      vertical-align: top;
	      overflow: auto;
	      height: 70vh; width:100%">

	        <?php foreach ($companys as $key => $value): ?>
	          <div style="border-bottom: 1px solid black;">
	              <p><?php echo $value->name; ?></p>
	              <a href="<?php echo "$pageurl?company=" . $value->tocken; ?>"><?php echo "$pageurl?company=" . $value->tocken; ?></a>
	              <form method="post" action="">
	                <fieldset>
	                  <?php wp_nonce_field($anatool_input_action, $anatool_input_field);?>
	                  <input type="hidden" value="<?php echo $value->id; ?>" name="companyid">
	                  <input type="hidden" value="<?php echo $value->name; ?>" name="scompanyname">
	                  <input type="submit" value="auswählen" name="submit_selectcompany_btn">
	                </fieldset>
	              </form>
	          </div>
	        <?php endforeach;?>
	      </div>
	      <?php
} else {
                $scompanyname = sanitize_text_field($_POST['scompanyname']);
                if ($scompanyname != "") {
                    $companyid = sanitize_text_field($_POST['companyid']);
                    self::setsysparam('selectcompanyid', $companyid);
                    echo "<script>location.reload();</script>";
                    //echo get_user_meta(get_current_user_id(), 'selectcompanyid',true)." Ausgewählt";
                }
            }
        }
        return ob_get_clean();
    }

    public function anatool_frontend_show_select_companys()
    {
        if (!is_user_logged_in()) {
            return null;
        }
        ob_start();
        $selectcompanyid = self::getsysparam('selectcompanyid');
        if ($selectcompanyid != null) {
            $company = $this->database->anatool_db_check_companyid($selectcompanyid[0]);
            if ($company != null) {
                //var_dump($company);
                $anatool_input_action = "anatoolclear";
                $anatool_input_field = "anatoolcompanyselectclear";

                if (!isset($_POST[$anatool_input_field])
                    || !wp_verify_nonce($_POST[$anatool_input_field], $anatool_input_action)) {
                    //$companys = $this->database->anatool_db_get_all_companys();
                    ?>
	          <div>
	              <p><?php echo $company->name; ?></p>
	              <form method="post" action="">
	                <fieldset>
	                  <?php wp_nonce_field($anatool_input_action, $anatool_input_field);?>
	                  <input type="submit" value="Auswahl löschen" name="submit_selectcompany_btn">
	                </fieldset>
	              </form>
	          </div>
	        <?php
} else {
                    delete_user_meta(get_current_user_id(), 'selectcompanyid');
                    echo "<script>location.reload();</script>";
                }
            }
        }
        return ob_get_clean();
    }

    public function anatool_frontend_create_groups()
    {
        if (!is_user_logged_in()) {
            return null;
        }
        ob_start();
        $companyid = self::getsysparam('selectcompanyid');
        if ($companyid != null) {
            $company = $this->database->anatool_db_check_companyid($companyid[0]);
            if ($company != null) {
                //var_dump($company);
                $anatool_input_action = "anatoolsavenewgroup";
                $anatool_input_field = "anatoolgroupsave";

                if (!isset($_POST[$anatool_input_field]) || !isset($_POST['usersname']) || !isset($_POST['usercount'])
                    || !wp_verify_nonce($_POST[$anatool_input_field], $anatool_input_action)) {
                    //print 'keine daten';
                    ?>
	        <div>
	          <form action="" method="POST">
	            <?php wp_nonce_field($anatool_input_action, $anatool_input_field);?>
	            <fieldset>
	                <div>
	                    <label for="usersname">Teamname</label><br>
	                    <input type="text" id="usersname" name="usersname"/>
	                </div>
	                <div>
	                    <label for="usercount">Anzahl</label><br>
	                    <input type="number" id="usercount" name="usercount"/>
	                </div>
	            </fieldset><br>
	            <div><input type="submit" value="Hinzufügen" name="submit_btn<?php echo uniqid(); ?>"></div>
	          </form>
	        <div>
	        <?php
} else {
                    //print 'save data';
                    $usersname = sanitize_text_field($_POST['usersname']);
                    $usercount = sanitize_text_field($_POST['usercount']);
                    $this->database->anatool_db_insert_group($usersname, $usercount, $company->id);
                    //echo "<script>location.reload();</script>";
                }
                //var_dump($_POST);

                if (isset($_POST[$anatool_input_field]) &&
                    isset($_POST['usersname']) &&
                    isset($_POST['usercount']) &&
                    wp_verify_nonce($_POST[$anatool_input_field], $anatool_input_action)) {
                    //$_POST = array();
                    //echo "<script>location.reload();</script>";
                    $id = uniqid();
                    echo "<form id='$id' action='' method='POST'><input type='submit' value=''></form>";
                    echo "<script>console.log('reload'); document.getElementById('$id').submit();</script>";
                }
            }
        }
        return ob_get_clean();
    }

    public function anatool_frontend_output_groups($atts)
    {
        if (!is_user_logged_in()) {
            return null;
        }

        $companyid = self::getsysparam('selectcompanyid');
        if ($companyid != null) {
            $company = $this->database->anatool_db_check_companyid($companyid[0]);
            if ($company != null) {

                ob_start();

                //echo htmlentities($_SERVER['PHP_SELF']);? >
                $groups = $this->database->anatool_db_get_all_groups($company->id);
                $adminsettings = get_option('anatool_option_name');
                $pageid = $adminsettings['evaluationdetail'];
                //var_dump($groups);

                ?>
	      <div style=" display: inline-block;
	      vertical-align: top;
	      overflow: auto;
	      height: 70vh; width:100%">
	        <?php foreach ($groups as $key => $value): ?>
	          <?php //var_dump($value);
                $saveuser = 0; //$this->database->anatool_db_get_group_saveuser($value->id);?>
	          <div style="border-bottom: 1px solid black;">
	              <p>Teamname: <?php echo $value->name ?></p>
	              <p>Probanden: <?php echo $value->usercount ?></p>
	              <p>Abgeschlossene: <?php echo $value->count ?></p>
	              <form method="post" action="<?php echo get_permalink($pageid); ?>">
	                <input type="hidden" value="<?php echo $value->id; ?>" name="gid">
	                <input type="submit" value="Auswertung" name="submit_btn">
	              </form>
	          </div>
	        <?php endforeach;?>
	      </div>
	      <?php

            }
        }
        return ob_get_clean();
    }

    public function anatool_frontend_show_token_company()
    {
        if (!isset($_GET['company'])) {
            return null;
        }

        $company = sanitize_text_field($_GET['company']);
        //echo $company;
        $adminsettings = get_option('anatool_option_name');
        //echo urlencode($_GET['company']);
        $tokens = $this->database->anatool_db_get_all_usertoken_by_company_token(urlencode($company));
        if (is_array($tokens)) {
            ?>
	    <div style=" display: inline-block;
	    vertical-align: top;
	    overflow: auto;
	    height: 70vh; width:100%">
	    <?php
			$lastgroupid = "";
            $lastgroupclose = "";
            foreach ($tokens as $key => $value): ?>
	      <?php if ($lastgroupid != $value->id): ?>
	          <?php echo $lastgroupclose . $value->name;
            $lastgroupclose = "</ul>";
            $lastgroupid = $value->id; ?><ul>
	      <?php endif;?>
	        <?php if ($value->ctocken != null && $value->utocken != null): ?>
	          <li>Nutzer:<ul><li>Link: <a href="<?php echo $adminsettings['limeurl'] . "" . $adminsettings['limeid'] . "?cid=" . $value->ctocken . "&uid=" . $value->utocken; ?>">
	            <?php echo $adminsettings['limeurl'] . "" . $adminsettings['limeid'] . "?cid=" . $value->ctocken . "&uid=" . $value->utocken; ?></a></li>
	          <li>Speicherdatum: <?php echo $value->savedate ?></li></ul></li>
	        <?php endif;?>
	    <?php endforeach;?>
	    <?php
}
    }

    public function anatool_frontend_chart($atts)
    {
        if (!is_user_logged_in()) {
            return null;
        }
        ob_start();
        $param = shortcode_atts(array(
            'allgroups' => "1",
            'selectgroups' => "1",
            'labels' => '',
            'questions' => '',
            'title' => '',
            'type' => 'horizontalBar',
            'height' => '100',
            'plots'=>'',
            'colorarea'=>'',
            'labelsx'=>''
        ), $atts);

        if ($param['type'] == 'bar' || $param['type'] == 'line' || $param['type'] == 'radar' || $param['type'] == 'horizontalBar') {} else {
            return null;
        }

        //if(!isset( $_POST['gid'])) return null;

        //$gid = $_POST['gid'];

        //$cid=1;
        //$gid =1;
        $companyid = self::getsysparam('selectcompanyid');
        if ($companyid == null) {
            return null;
        }

        if (count($companyid) < 1) {
            return null;
        }

        $companyid = $companyid[0];
        $uid = get_current_user_id();
        //if($this->database->anatool_db_check_gid_by_user($gid,$uid)<=0) return null;

        $plots = explode(";", $param['plots']);

        $colorarea =  explode(";", $param['colorarea']);

        $labelsx =  explode(";", $param['labelsx']);

        $label = explode(";", $param['labels']);
        $questions = explode(";", $param['questions']);

        if (count($label) <= 0 || count($questions) <= 0 || count($label) != count($questions)) {
            return null;
        }

        $questiongroups = array();
        foreach ($questions as $key => $questionstr) {
            $questiongroups[] = explode(",", $questionstr);
        }

        //print_r($questiongroups);

        $avgquestions = $this->database->anatool_db_get_company_saveuser_questions_groups_by_id($questiongroups, $companyid); //anatool_db_get_group_saveuser_questions_groups_by_id($questiongroups,$gid);
        $avggroupsquestions = $this->database->anatool_db_get_allcompanys_saveuser_questions_groups_by_id($questiongroups); //anatool_db_get_allgroup_saveuser_questions_groups_by_id($questiongroups,$gid);
        // print_r($avgquestions);echo "<br><br>";
        //print_r($avggroupsquestions);echo "<br><br>";

        if (count($avgquestions) <= 0 || count($avggroupsquestions) <= 0) {
            return null;
        }

        $chartid = uniqid();
        ?>
    <div  class="anatoolchartwarp" style="  width: 100%">
      <canvas id="anatoolchart<?php echo $chartid; ?>" height="<?php echo $param['height']; ?>"></canvas>
    </div>
	  <script>
	  var ctx<?php echo $chartid; ?> = document.getElementById("anatoolchart<?php echo $chartid; ?>").getContext('2d');
		Chart.defaults.global.tooltips.enabled = false;
	  var myChart<?php echo $chartid; ?> = new Chart(ctx<?php echo $chartid; ?>, {
	      type: '<?php echo $param['type']; ?>',
	      data: {
	          labels:  <?php echo json_encode($label); ?>,//["Red", "Blue", "Yellow", "Green", "Purple", "Orange"],
	          datasets: [<?php if ($param['selectgroups'] == "1"): ?>{
	              data: <?php echo json_encode($avgquestions); ?>,//[12, 19, 3, 5, 2, 3]
								backgroundColor: [
									<?php if (count($avgquestions) <= 1): ?>
	               'rgba(150, 150, 150, 1)'// 'rgba(255, 99, 132, 1)',
									<?php elseif ($param['type'] == 'radar'): ?>
									'rgba(255, 99, 132, 0.1)'
									<?php else: ?>
									// 'rgba(54, 162, 235, 1)',
										    //             'rgba(255, 99, 132, 1)',
										    //             'rgba(255, 206, 86, 1)',
										    //             'rgba(75, 192, 192, 1)',
										    //             'rgba(153, 102, 255, 1)',
										    //             'rgba(255, 159, 64, 1)'
										    				'rgba(150, 150, 150, 1)','rgba(150, 150, 150, 1)','rgba(150, 150, 150, 1)','rgba(150, 150, 150, 1)','rgba(150, 150, 150, 1)','rgba(150, 150, 150, 1)','rgba(150, 150, 150, 1)','rgba(150, 150, 150, 1)','rgba(150, 150, 150, 1)','rgba(150, 150, 150, 1)','rgba(150, 150, 150, 1)'

									<?php endif;?>
            		]
								<?php if ($param['type'] == 'radar'): ?>,borderColor:[
									'rgba(150, 150, 150, 1)'//'rgba(255, 99, 132, 1)'
								]
								<?php endif;?>
	          },<?php endif;?><?php if ($param['allgroups'] == "1"): ?>
	          {
	              data: <?php echo json_encode($avggroupsquestions); ?>,//[12, 19, 3, 5, 2, 3]
								backgroundColor: [
									<?php if ($param['type'] == 'radar'): ?>
									'rgba(54, 162, 235, 0.1)'
									<?php else: ?>
									// 'rgba(54, 162, 235, 1)',
					    //             'rgba(255, 99, 132, 1)',
					    //             'rgba(255, 206, 86, 1)',
					    //             'rgba(75, 192, 192, 1)',
					    //             'rgba(153, 102, 255, 1)',
					    //             'rgba(255, 159, 64, 1)'
					    				'rgba(210, 210, 210, 1)','rgba(210, 210, 210, 1)','rgba(210, 210, 210, 1)','rgba(210, 210, 210, 1)','rgba(210, 210, 210, 1)','rgba(210, 210, 210, 1)','rgba(210, 210, 210, 1)','rgba(210, 210, 210, 1)','rgba(210, 210, 210, 1)','rgba(210, 210, 210, 1)','rgba(210, 210, 210, 1)'
									<?php endif;?>
            		]
								<?php if ($param['type'] == 'radar'): ?>,borderColor:[
									'rgba(54, 162, 235, 1)'
								]
								<?php endif;?>
	          }<?php endif;?>
	        ]
	      },
				plugins: [{
                    <?php if ($param['type'] == 'horizontalBar'): ?>

                    // beforeInit: function(chart) {
                    //     chart.data.labels.forEach(function(e, i, a) {
                    //         console.log(e);                  
                    //         a[i] = e.split(' ');
                    //         console.log(a[i]);
                    //         console.log(Math.round(a[i].length));
                    //         var mid = Math.round(a[i].length/2);
                    //         var one = a[i].slice(0, mid);
                    //         one = one.join(' ');
                    //         var two = a[i].slice(mid, a[i].length);
                    //         two = two.join(' ');

                    //         a[i] = new Array(one,two);
                    //         console.log(a[i]);
                    //         // a[i] = e.split(/ (.+)/)
                    //     });
                    // },



			    /*afterUpdate: function(chart) {
			      var dataset = chart.config.data.datasets[0];
			      var offset = -120;

			      for (var i = 0; i < dataset._meta[0].data.length; i++) {
			        var model = dataset._meta[0].data[i]._model;
			        model.x += offset;
			        model.controlPointNextX += offset;
			        model.controlPointPreviousX += offset;
			      }*/

			      /*var dataset2 = chart.config.data.datasets[1];
			      var offset2 = 20;

			      for (var o = 0; o < dataset2._meta[0].data.length; o++) {
			        var model2 = dataset2._meta[0].data[o]._model;
			        model2.x += offset2;
			        model2.controlPointNextX += offset2;
			        model2.controlPointPreviousX += offset2;
			      }
                },*/
                <?php if(count($colorarea)>1):?>
					beforeDraw: function (chart, easing) {
						if (chart.config.options.chartArea) if(chart.config.options.chartArea.backgroundColor) {
							var helpers = Chart.helpers;
							var ctx = chart.chart.ctx;
							var chartArea = chart.chartArea;

							ctx.save();
							let coloralpha ="0.9";
							// console.log("chartArea.left",chartArea.left);
							// console.log("chartArea.right",chartArea.right);
							// console.log("chartArea.bottom",chartArea.bottom);
							// console.log("chartArea.top",chartArea.top);
							//var grd=ctx.createLinearGradient(chartArea.left,chartArea.top,chartArea.right - chartArea.left,chartArea.bottom - chartArea.top);
							var grd=ctx.createLinearGradient(chartArea.left,chartArea.top,chartArea.right,chartArea.top);

                            <?php //$colorarea = ["0:g","0.5:r","1:y"] ?>
                            <?php foreach ($colorarea as $key=>$value) : ?>
                            <?php $tmpvaluecolor = explode(":", $value); ?>
                            <?php if(count($tmpvaluecolor)==2):?>

                                <?php if($tmpvaluecolor[1] == 'g'):?>
                                grd.addColorStop(<?php echo $tmpvaluecolor[0];?> ,"rgba(0, 193, 0, "+coloralpha+")");
                                <?php endif;?>
                                <?php if($tmpvaluecolor[1] == 'y'):?>
                                grd.addColorStop(<?php echo $tmpvaluecolor[0];?>,"rgba(255, 255, 40, "+coloralpha+")");
                                <?php endif;?>
                                <?php if($tmpvaluecolor[1] == 'r'):?>
                                grd.addColorStop(<?php echo $tmpvaluecolor[0];?>,"rgba(255, 0, 0, "+coloralpha+")");
                                <?php endif;?>
                            
                            <?php endif;?>
                            <?php endforeach; ?>

							// grd.addColorStop(0,"rgba(0, 193, 0, "+coloralpha+")");
							// grd.addColorStop(0.5,"rgba(255, 255, 40, "+coloralpha+")");
							// grd.addColorStop(1,"rgba(255, 0, 0, "+coloralpha+")");

							ctx.fillStyle = grd;
							//ctx.fillStyle = chart.config.options.chartArea.backgroundColor;
							ctx.fillRect(chartArea.left, chartArea.top, chartArea.right - chartArea.left, chartArea.bottom - chartArea.top);
							ctx.restore();
						}
                    },
                    <?php endif;?>
				<?php endif;?>
				<?php if ($param['type'] != 'radar'): ?>
					afterDraw: function(chartInstance) {
						var ctx = chartInstance.chart.ctx;

						// render the value of the chart above the bar
						ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, 'normal', Chart.defaults.global.defaultFontFamily);
						ctx.textAlign = 'center';
						ctx.textBaseline = 'bottom';

						chartInstance.data.datasets.forEach(function (dataset) {
								for (var i = 0; i < dataset.data.length; i++) {
										var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model;
										let number = (dataset.data[i]).toFixed(1);
										if(number>=0)
										ctx.fillText(number, model.x -20, model.y+7);
								}
						});
					}<?php endif;?>
			  }],
	      options: {
					annotation: {
						annotations: [
                            <?php if(count($colorarea)>1):?>
                            <?php foreach ($plots as $key=>$value) : ?>
							{
								drawTime: "afterDatasetsDraw",
								type: "line",
								mode: "vertical",
								scaleID: "x-axis-0",
								value: /*2.5*/<?php echo $value;?>,
								borderWidth: 2,
								borderDash: [10, 10],
								borderColor: "gray",
								// label: {
								// 	content: "TODAY",
								// 	enabled: true,
								// 	position: "top"
								// }
                            },
                            <?php endforeach; ?>
                            <?php endif; ?>
							/*{
								drawTime: "afterDatasetsDraw",
								type: "line",
								mode: "vertical",
								scaleID: "x-axis-0",
								value: 3.5,
								borderWidth: 2,
								borderDash: [10, 10],
								borderColor: "gray",
								// label: {
								// 	content: "TODAY",
								// 	enabled: true,
								// 	position: "top"
								// }
							}*/
						]
					},
					<?php if ($param['type'] == 'horizontalBar'): ?>
					chartArea: {
           backgroundColor: 'rgba(251, 85, 85, 0.4)'
        	},<?php endif;?>
	        legend: {
	            display: false
	          },
						<?php if ($param['type'] == 'radar'): ?>
						scale: {
								ticks: {
										beginAtZero: true,
										max: 5,
										stepSize:1
								}
						}
						<?php else: ?>
	          scales: {
	              yAxes: [{
	                  ticks: {
                          beginAtZero:true
	                  }
	              }],
	              xAxes: [{
	                  ticks: {
												min:1,
												max:5,
	                      beginAtZero:true,
                                                stepSize:1.0,
                        
                            <?php if(count($labelsx)>1):?>
                             callback: function(value, index, values) {
                                $labels = <?php echo json_encode($labelsx); ?>;
                                return $labels[value];
                            }
                            <?php endif;?>
	                  }
	              }]
	          }
					<?php endif;?>
	      }
	  });</script>

	  <?php
$eout = ob_get_clean();
        //var_dump($eout);
        return $eout;
    }

    public function anatool_frontend_inner_ifblock($atts, $content)
    {
        return self::anatool_frontend_ifblock($atts, $content);
    }

    public function anatool_frontend_ifblock($atts, $content)
    {
        if (!is_user_logged_in()) {
            return null;
        }
        ob_start();
        $param = shortcode_atts(array(
            'questions' => '',
            'max' => '',
            'operator' => '',
        ), $atts);
        //if(!isset( $_POST['gid'])) return null;

        //$gid = $_POST['gid'];

        //$gid =1;
        $companyid = self::getsysparam('selectcompanyid');
        //var_dump($companyid);
        if ($companyid == null) {
            return null;
        }

        if (count($companyid) < 1) {
            return null;
        }

        $companyid = $companyid[0];
        $uid = get_current_user_id();
        //if($this->database->anatool_db_check_gid_by_user($gid,$uid)<=0) return null;

        if ($param['questions'] != '' && $param['max'] != '' && $param['operator'] != '') {
            $questions = explode(";", $param['questions']);
            if (count($questions) <= 0 || count($questions) > 1) {
                return null;
            }

            $questiongroups = array();
            foreach ($questions as $key => $questionstr) {
                $questiongroups[] = explode(",", $questionstr);
            }
            $avgquestions = $this->database->anatool_db_get_company_saveuser_questions_groups_by_id($questiongroups, $companyid); //$avgquestions= $this->database->anatool_db_get_group_saveuser_questions_groups_by_id($questiongroups,$gid);
            //var_dump($avgquestions);
            if (count($avgquestions) <= 0 || count($avgquestions) > 1) {
                return null;
            }

            if ($avgquestions[0] == -1) {
                return null;
            }

            //var_dump($param);

            if (strpos($param['operator'], '&gt') !== false) { //>
                //echo "bigger $avgquestions[0]  ".floatval($param['max']);
                if ($avgquestions[0] > floatval($param['max'])) {
                    //echo "bigger immernoch";
                    echo do_shortcode($content);
                }
            } else if (strpos($param['operator'], '&lt') !== false) { //<
                //echo "smaller $avgquestions[0]";
                if ($avgquestions[0] < floatval($param['max'])) {
                    echo do_shortcode($content);
                }
            }

        }
        return ob_get_clean();
    }

    public function anatool_frontend_value($atts, $content)
    {
        if (!is_user_logged_in()) {
            return null;
        }
        ob_start();
        $param = shortcode_atts(array(
            'questions' => '',
            'max' => '',
            'operator' => '',
        ), $atts);

        //var_dump($param);
        //if(!isset( $_POST['gid'])) return null;

        //$gid = $_POST['gid'];

        //$gid =1;
        $companyid = self::getsysparam('selectcompanyid');
        if ($companyid == null) {
            return null;
        }

        if (count($companyid) < 1) {
            return null;
        }

        $companyid = $companyid[0];
        $uid = get_current_user_id();
        //if($this->database->anatool_db_check_gid_by_user($gid,$uid)<=0) return null;

        if ($param['questions'] != '') {
            $questions = explode(";", $param['questions']);
            if (count($questions) <= 0 || count($questions) > 1) {
                return null;
            }

            $questiongroups = array();
            foreach ($questions as $key => $questionstr) {
                $questiongroups[] = explode(",", $questionstr);
            }

            $avgquestions = $this->database->anatool_db_get_company_saveuser_questions_groups_by_id($questiongroups, $companyid); //$avgquestions= $this->database->anatool_db_get_group_saveuser_questions_groups_by_id($questiongroups,$gid);
            //var_dump($avgquestions);
            if (count($avgquestions) <= 0 || count($avgquestions) > 1) {
                return null;
            }

            //if($avgquestions[0]=-1)return null;
            echo round($avgquestions[0], 1);
        }
        return ob_get_clean();
    }

    public function setsysparam($name, $value)
    {
        $havemeta = get_user_meta(get_current_user_id(), $name, false);
        if ($havemeta) {
            //echo $havemeta;
            update_user_meta(get_current_user_id(), $name, $value);
        } else {
            add_user_meta(get_current_user_id(), $name, $value);
        }
        return $value;
    }

    public function getsysparam($name)
    {
        $havemeta = get_user_meta(get_current_user_id(), $name, false);
        if ($havemeta) {
            return $havemeta;
        }
        return null;
    }

    public function insertbenchmark()
    {

        if (!is_user_logged_in()) {
            return null;
        }
        ob_start();

        $this->database->insertbenchmark();
        //$this->database->deletebench();

        return ob_get_clean();
    }

    public function removebenchmark()
    {
        if (!is_user_logged_in()) {
            return null;
        }
        ob_start();

        $this->database->deletebench();

        return ob_get_clean();
    }

    public function twigtest()
    {
        $twig = Analysetool_With_Limesurvey_Twig::getInstance()->twig;
        ob_start();

        $current_user = wp_get_current_user();
        if (0 == $current_user->ID) {
            echo $twig->render('notloggedin.html');
            return;
        }

        $title = "";

        $nonceaction = "test" . 'submit';
        $noncefield = 'teststring';

        if (!isset($_POST[$noncefield]) ||
            !isset($_POST['title']) ||
            !wp_verify_nonce($_POST[$noncefield], $nonceaction)) {
            print 'Sorry, your nonce did not verify.';
            //exit;
        } else {
            $title = sanitize_text_field($_POST['title']);
            echo $twig->render('testview.html', [
                'name' => $current_user->user_login,
                'mail' => $current_user->user_email,
                'first_name' => $current_user->user_firstname,
                'last_name' => $current_user->user_lastname,
                'display_name' => $current_user->display_name,
                'id' => $current_user->ID,
                'title' => $title,
                'submitnonce' => wp_nonce_field($nonceaction, $noncefield, true, false),
                'categories' => [1, 2, 3, 4],
            ]);
        }

    }

    public function show_tokenlist_company()
    {
        if (!isset($_GET['company'])) {
            return null;
		}
		ob_start();

        $company = sanitize_text_field($_GET['company']);
        //echo $company;
        //var_dump($company);
        $adminsettings = get_option('anatool_option_name');
        //echo urlencode($_GET['company']);
        $tokens = $this->database->anatool_db_get_all_usertoken_by_company_token(urlencode($company));
        if (is_array($tokens)) {
			//var_dump($tokens);
			$twig = Analysetool_With_Limesurvey_Twig::getInstance()->twig;
            $limeurl = $adminsettings['limeurl'] . "" . $adminsettings['limeid'] . "?cid=";
			$twig = Analysetool_With_Limesurvey_Twig::getInstance()->twig;
			$mailtext = str_replace(array("\r"), '%0D%0A', $adminsettings['mailtext_user']);
            echo $twig->render('linklist.html', ['mailtext'=> $mailtext,'users' => $tokens, 'limeurl' => $limeurl, 'back' => false]);
		}
		return ob_get_clean();
    }

    public function analysetool_dashboard()
    {
        ob_start();

        if (isset($_GET['company']) && !isset($_GET['dachboardstatus'])) {
            //do_shortcode('anatool_show_token_company');
            echo $this->show_tokenlist_company();
            return ob_get_clean();
        }

        if (!$this->checklogin()) {
            return ob_get_clean();
        }

        $twig = Analysetool_With_Limesurvey_Twig::getInstance()->twig;
        $userid = get_current_user_id();
        $status = '0';
        $companyid = get_user_meta($userid, "companyid", true);
        $analysetoolstatusmetakey = "analysetoolstatus";
        $analysecompanyidmetakey = "companyid";
        $adminsettings = get_option('anatool_option_name');

        $nonceaction_status = "status" . 'submit';
        $noncefield_status = 'status';

        $checkstatus = get_user_meta($userid, $analysetoolstatusmetakey, true);
        if (empty($checkstatus)) {
            update_user_meta($userid, $analysetoolstatusmetakey, $status);
        } else {
            $status = $checkstatus;
        }

        if (!isset($_POST[$noncefield_status]) ||
            !wp_verify_nonce($_POST[$noncefield_status], $nonceaction_status)) {
            //echo "nounce empty";
        } else {

            if (isset($_POST['dachboardstatus'])) {
                $status = sanitize_text_field($_POST['dachboardstatus']);
                update_user_meta($userid, $analysetoolstatusmetakey, $status);
            }

            //check company
            if (isset($_POST['companyid'])) {
                $companyid_input = intval(sanitize_text_field($_POST['companyid']));
                $companyid_db = $this->database->anatool_db_check_companyid($companyid_input);
                if ($companyid_db != null) {
                    update_user_meta($userid, $analysecompanyidmetakey, $companyid_input);
                    $companyid = $companyid_db->id;
                }
            }

            //insert firma
            if (isset($_POST['companyname'])) {
                $companyname = sanitize_text_field($_POST['companyname']);
                $this->database->anatool_db_insert_frima($companyname);
            }

            //insert firma multi
            if (isset($_POST['companynamemulti']) && isset($_POST['companymultiid'])) {
                $companyname = sanitize_text_field($_POST['companynamemulti']);
                $quizid =  intval(sanitize_text_field($_POST['companymultiid']));
                $this->database->anatool_db_insert_frima_quiz($companyname,$quizid);
            }

            //insert team
            if (isset($_POST['teamname']) && isset($_POST['teamcount'])) {
                $teamname = sanitize_text_field($_POST['teamname']);
                $teamcount = intval(sanitize_text_field($_POST['teamcount']));
                $this->database->anatool_db_insert_group($teamname, $teamcount, $companyid);
            }
        }

        if($status == 2 && !isset($_POST['companytocken'])){
            update_user_meta($userid, $analysetoolstatusmetakey, 0);
            $status = 0;
        }

        switch ($status) {
            default:
            case '0':{

                    if(isset($_POST['clearcompany'])&&isset($_POST['clearcompanyid'])){
                        $clearcompanyid = intval(sanitize_text_field($_POST['clearcompanyid']));
                        //var_dump("add member");
                        $this->database->deletecompanydata($clearcompanyid);
                    }
                    //$companys = $this->database->anatool_db_get_all_companys();
                    $companys = $this->database->anatool_db_get_all_companys_savecount();


                    $pageid = $adminsettings['companytokenlist'];
                    $pageurl = get_permalink($pageid);

                    $evaluationdetailpageid = $adminsettings['evaluationdetail'];
                    $evaluationdetailpageurl = get_permalink($evaluationdetailpageid);

                    for ($i = 0; $i < count($companys); $i++) {
                        $companys[$i]->showdetail= $companys[$i]->savecount>=$adminsettings['minical']?true:false;
                        $companys[$i]->url = "$pageurl?company=" . $companys[$i]->tocken;
                        $companys[$i]->tocken = base64_encode($companys[$i]->tocken);
                    }
                    $adminmode =false;
                    if(current_user_can('administrator')) $adminmode=true;

                    $mailtext = str_replace(array("\r"), '%0D%0A', $adminsettings['mailtext']);

                    $multiids = $this->getlimeids();

                    //var_dump($companys);
                    echo $twig->render('companys_overview.html', ['multiids'=>$multiids,'mailtext'=>$mailtext,'adminmode'=> $adminmode,'evaluationurl' => $evaluationdetailpageurl, "companys" => $companys,
                        'submitnoncestatus' => wp_nonce_field($nonceaction_status, $noncefield_status, true, false)]);
                }break;
            case '1':{
                    //if($companyid!=""){
					if(isset($_POST['addteammember'])&&isset($_POST['addteammemberid'])){
						$tmpgroupid = intval(sanitize_text_field($_POST['addteammemberid']));
						//var_dump("add member");
						$this->database->anatool_db_insert_member2group($tmpgroupid );
					}

					$groups = $this->database->anatool_db_get_all_groups2($companyid);
                    // var_dump($groups);
                    echo $twig->render('teams_overview.html', ['groups' => $groups,
                        'submitnoncestatus' => wp_nonce_field($nonceaction_status, $noncefield_status, true, false)]);
                    // }else{
                    //
                    // }
                }break;
            case '2':{
					if(!isset($_POST['companytocken'])){
                        //continue;
                        //break;
                        update_user_meta($userid, $analysetoolstatusmetakey, 0);
					}else{
                        
                        $comptoken="";
                        if(isset($_POST['clearuser'])&&isset($_POST['clearuserid'])){
                            $clearuserid = intval(sanitize_text_field($_POST['clearuserid']));
                            //var_dump("add member");
                            $this->database->deleteuserdata($clearuserid);
                        }

                        $companytocken = base64_decode(sanitize_text_field($_POST['companytocken']));
                        $comptoken=$_POST['companytocken'];
						$usertokens = $this->database->anatool_db_get_all_usertoken_by_company_token($companytocken);
						$mailtext =  str_replace(array("\r"), '%0D%0A', $adminsettings['mailtext_user']);
						// var_dump($usertokens);
						if (is_array($usertokens)) {
                            //var_dump($tokens);
                            $adminmode =false;
                            if(current_user_can('administrator')) $adminmode=true;
							$limeurl = $adminsettings['limeurl'] . "" . $adminsettings['limeid'] . "?cid=";
							echo $twig->render('linklist_overview.html', ['comptocken'=>$comptoken,'compid'=> $companyid, 'adminmode'=> $adminmode,'mailtext'=> $mailtext,'users' => $usertokens, 'limeurl' => $limeurl, 'back' => true, 'submitnoncestatus' => wp_nonce_field($nonceaction_status, $noncefield_status, true, false)]);
						}
					}
                }break;
            case '3':{
                    update_user_meta($userid, 'selectcompanyid', $companyid);
                    update_user_meta($userid, $analysetoolstatusmetakey, 0);

                    $detailpageurl = get_permalink($adminsettings['evaluationdetail']);

                    echo "<script>window.location.href = '$detailpageurl';</script>";

                }break;
            
        }

        return ob_get_clean();
    }

    public function analysetool_linklist()
    {

    }

    public function checklogin()
    {
        $twig = Analysetool_With_Limesurvey_Twig::getInstance()->twig;
        $current_user = wp_get_current_user();
        if (0 == $current_user->ID) {
            echo $twig->render('notloggedin.html');
            return false;
        }
        return true;
    }

    public function getlimeids(){
        $adminsettings = get_option('anatool_option_name');
        //$idsraw= $adminsettings['limemultiid'];

        $result = array();
        // foreach (explode(PHP_EOL, $idsraw) as $piece) {
        //     $result[] = explode(';', $piece);
        // }
        return $result;
    }

    public function showCompanyInfo(){

        $twig = Analysetool_With_Limesurvey_Twig::getInstance()->twig;


            if (!is_user_logged_in()) {
            return null;
        }
        ob_start();
            
        $companyid = self::getsysparam('selectcompanyid');
        //var_dump($companyid);
        if ($companyid == null) {
            return null;
        }

        if (count($companyid) < 1) {
            return null;
        }

        $companys = $this->database->anatool_db_get_all_companys();

        $companyid = $companyid[0];
        $uid = get_current_user_id();
        
        $numberOfParticipants = $this->database->getCompanyParticipants($companyid);
        $allNumberOfParticipants = $this->database->CountQSave();

        $currentcp ="";
        foreach ($companys as  $value){
            if($value->id == $companyid){
                $currentcp = $value;
            }
                }
        echo $twig->render('auswertung_header_info.html', [
                'date' => date("d/m/Y"),
                'numberOfParticipants' => $numberOfParticipants,
                'allNumberOfParticipants' => $allNumberOfParticipants,
                'companyname' => $currentcp,

            ]);
            
        return ob_get_clean();

    }


}
