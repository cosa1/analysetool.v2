(function( $ ) {
	'use strict';



// 	$(function() {
// 		$('head style[type="text/css"]').attr('type', 'text/less');
//
// 		// Step 2: Set development mode to see errors
// 		less.env = 'development';
// 		less.watch();
//
// 		// Step 3: Tell Less to refresh the styles
// 		less.refreshStyles();
// });

$(function() {
	setTimeout(function(){
  M.AutoInit();
  console.log("materilize");
  toastr.options = {
    "closeButton": false,
    "debug": false,
    "newestOnTop": false,
    "progressBar": true,
    "positionClass": "toast-top-right",
    "preventDuplicates": false,
    "onclick": null,
    "showDuration": "300",
    "hideDuration": "1000",
    "timeOut": "5000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
  }
  // toastr.options = {
  //   "closeButton": true,
  //   "debug": false,
  //   "newestOnTop": false,
  //   "progressBar": true,
  //   "positionClass": "toast-top-center",
  //   "preventDuplicates": false,
  //   "onclick": null,
  //   "showDuration": "300",
  //   "hideDuration": "1000",
  //   "timeOut": "5000",
  //   "extendedTimeOut": "1000",
  //   "showEasing": "swing",
  //   "hideEasing": "linear",
  //   "showMethod": "fadeIn",
  //   "hideMethod": "fadeOut"
  // }
}, 2000);

});


$(document).ready(function() {
  $('.modal').modal();

  function copyToClipboard(element) {
    var $temp = $("<input>");
    $("body").append($temp);
    $temp.val($(element).text()).select();
    document.execCommand("copy");
    $temp.remove();
    console.log("copy to clipboard");
    toastr.success('Link in die Zwischenablage kopiert')
    // M.toast({
    //   html: '<h4>Link in die Zwischenablage kopiert</h4>',
    //   classes: 'rounded'
    // })
  }

  $(document).on("click", ".linkcopyicon", function() {
    console.log($(this).next().text());
    copyToClipboard($(this).next());
  });

  $(document).on("click", ".qrcodeicon", function() {
    console.log($(this).next().text());
		$('#qrcode').empty();
    $('#qrcode').qrcode("" + $(this).next().text());
    $('#qrcodetext').empty();
    $('#qrcodetext').append("" + $(this).next().text());

    $('#modalqr').modal('open');
  });

  $(document).on("click", "#mailsubmitdialog", function() {
    $('#modalmail').modal('close');
  });

  $(document).on("click", "#qrcodedownload button", function() {
    // $('#qrcode canvas').toDataURL("image/png").replace("image/png", "image/octet-stream");
    var image = document.querySelector('#qrcode canvas').toDataURL("image/png").replace("image/png", "image/octet-stream");
    var download = document.getElementById("qrcodedownload");
    download.setAttribute("href", image);
  });
  $('body').chardinJs();
  
  $(document).on("click", ".chardinhelp", function() {

    return ($('body').data('chardinJs')).toggle();
  });

 
  $(document).on("click", ".enjoyhinthelp", function() {
    var enjoyhint_instance = new EnjoyHint({});
    var enjoyhint_script_steps = [
      {
        'click .addcompanyicon' : 'Klick auf den "Plus Button", um ein Unternehmen anzulegen',
        'skipButton' : {className: "mySkip", text: "Abbrechen"},
        'nextButton' : {className: "myNext", text: "Weiter"}
      },
      {
        'next #companyname' : 'Gibt den "Namen" des Unternehmens hier ein',
        'skipButton' : {className: "mySkip", text: "Abbrechen"},
        'nextButton' : {className: "myNext", text: "Weiter"}
      },
      {
        'click .enjoyhintcreatecompany' : 'Klick auf "Speichern", um das Unternehmen anzulegen',
        'skipButton' : {className: "mySkip", text: "Abbrechen"},
        'nextButton' : {className: "myNext", text: "Weiter"}
      }
      // {
      //   onBeforeStart: function(){
      //     $('#modal1 #companyname').keydown(function(e){
      //       if(e.keyCode == 13 && $(this).val() !== ''){
      //         enjoyhint_instance.trigger('new_todo');
      //       }
      //     });
      //   },
      //   selector:'#modal1 #companyname',
      //   event:'new_todo',
      //   event_type:'custom',
      //   description:'Name eingeben und Enter drücken.',
      // }
    ];
    
    //set script config
    enjoyhint_instance.set(enjoyhint_script_steps);
    
    //run Enjoyhint script
    enjoyhint_instance.run();
  });
  /*
comanylinklist
comanylinklistcopy
comanylinklistqr -> qrcodedownload -> qrcodedownloadclose
comanylinklistmail
comanylinklistteams
comanylinklistauswertung

  */

  $(document).on("click", ".enjoyhinthelp2", function() {
    var enjoyhint_instance = new EnjoyHint({});
    var enjoyhint_script_steps = [
      {
        'click .comanylinklistcopy' : 'Klick das "Icon" an, um den Link in die Zwischenablage zu kopieren',
        'skipButton' : {className: "mySkip", text: "Abbrechen"},
        'nextButton' : {className: "myNext", text: "Weiter"} 
      },
      {
        'click .comanylinklistqr' : 'Klick das "Icon" an, um den Link als QR Code zu speichern' ,
        'skipButton' : {className: "mySkip", text: "Abbrechen"},
        'nextButton' : {className: "myNext", text: "Weiter"}
      },
      {
        'click #qrcodedownload' : 'Klick "Download" an, um den QR Code als Bild zu speichern' ,
        'skipButton' : {className: "mySkip", text: "Abbrechen"},
        'nextButton' : {className: "myNext", text: "Weiter"}
      },
      {
        'click .qrcodedownloadclose' : 'Klick "Schließen", um das Fenster zu schließen' ,
        'skipButton' : {className: "mySkip", text: "Abbrechen"},
        'nextButton' : {className: "myNext", text: "Weiter"}
      },
      {
        'click .comanylinklistmail' : 'Klick das "Icon" an, um die Mail an Betriebsinhaber zu versenden' ,
        'skipButton' : {className: "mySkip", text: "Abbrechen"},
        'nextButton' : {className: "myNext", text: "Weiter"}
      },
      {
        'click .comanylinklistteams' : 'Klick das "Icon" an, um Teams anzulegen' ,
        'skipButton' : {className: "mySkip", text: "Abbrechen"},
        'nextButton' : {className: "myNext", text: "Weiter"}
      }
    ];
    enjoyhint_instance.set(enjoyhint_script_steps);
    enjoyhint_instance.run();
  });

  $(document).on("click", ".enjoyhinthelp3", function() {
    var enjoyhint_instance = new EnjoyHint({});
    var enjoyhint_script_steps = [
      {
        'click .comanylinklist' : 'Klick den "Namen" des Unternehmens an, um auf die Seite mit der Linkübersicht zu gelangen' ,
        'skipButton' : {className: "mySkip", text: "Abbrechen"},
        'nextButton' : {className: "myNext", text: "Weiter"}
      }
    ];
    enjoyhint_instance.set(enjoyhint_script_steps);
    enjoyhint_instance.run();
  });

  $(document).on("click", ".enjoyhinthelp4", function() {
    var enjoyhint_instance = new EnjoyHint({});
    var enjoyhint_script_steps = [
      {
        'click .comanylinklistauswertung' : 'Klick das "Icon" an, um auf die Seite mit der Auswertung zu gelangen' ,
        'skipButton' : {className: "mySkip", text: "Abbrechen"},
        'nextButton' : {className: "myNext", text: "Weiter"}
      }
    ];
    enjoyhint_instance.set(enjoyhint_script_steps);
    enjoyhint_instance.run();
  });


  /*
  enjoyhint_linklist_copy_link
  enjoyhint_linklist_copy_link_qr
  enjoyhint_linklist_mail
  */

  $(document).on("click", ".enjoyhinthelplink", function() {
    var enjoyhint_instance = new EnjoyHint({});
    var enjoyhint_script_steps = [
      {
        'click .enjoyhint_linklist_copy_link' : 'Klick das "Icon" an, um den Link in die Zwischenablage zu kopieren' ,
        'skipButton' : {className: "mySkip", text: "Abbrechen"},
        'nextButton' : {className: "myNext", text: "Weiter"}
      },
      {
        'click .enjoyhint_linklist_copy_link_qr' : 'Klick das "Icon" an, um den Link als QR Code zu speichern' ,
        'skipButton' : {className: "mySkip", text: "Abbrechen"},
        'nextButton' : {className: "myNext", text: "Weiter"}
      },
      {
        'click #qrcodedownload' : 'Klick "Download", um den QR Code als Bild zu speichern' ,
        'skipButton' : {className: "mySkip", text: "Abbrechen"},
        'nextButton' : {className: "myNext", text: "Weiter"}
      },
      {
        'click .qrcodedownloadclose' : 'Klick "Schließen", um das Fenster zu schließen' ,
        'skipButton' : {className: "mySkip", text: "Abbrechen"},
        'nextButton' : {className: "myNext", text: "Weiter"}
      },
      {
        'click .enjoyhint_linklist_mail' : 'Klick das "Icon" an, um die Mail an den Mitarbeiter zu versenden' ,
        'skipButton' : {className: "mySkip", text: "Abbrechen"},
        'nextButton' : {className: "myNext", text: "Weiter"}
      },
      {
        'click .linklistbackbutton' : 'Klick das "Icon" an, um zur Firmenübersicht zurück zu gelangen',
        'skipButton' : {className: "mySkip", text: "Abbrechen"},
        'nextButton' : {className: "myNext", text: "Weiter"} 
      }
    ];
    enjoyhint_instance.set(enjoyhint_script_steps);
    enjoyhint_instance.run();
  });

  
  $(document).on("click", ".enjoyhinthelpteams", function() {
    var enjoyhint_instance = new EnjoyHint({});
    var enjoyhint_script_steps = [
      {
        'click .enjoyhintnewteamadd' : 'Klick das "Icon" an, um ein neues Team anzulegen' ,
        'skipButton' : {className: "mySkip", text: "Abbrechen"},
        'nextButton' : {className: "myNext", text: "Weiter"}
      },
      {
        'next #teamname' : 'Schreibe den "Name" des Teams in das Feld' ,
        'skipButton' : {className: "mySkip", text: "Abbrechen"},
        'nextButton' : {className: "myNext", text: "Weiter"}
      },
      {
        'next #teamcount' : 'Schreibe die "Anzahl" der Teammitglieder in das Feld',
        'skipButton' : {className: "mySkip", text: "Abbrechen"},
        'nextButton' : {className: "myNext", text: "Weiter"}
      },
      {
        'click .enjoyhinthelpteamsaddsave' : 'Klicke auf "Speichern", um das Team anzulegen',
        'skipButton' : {className: "mySkip", text: "Abbrechen"},
        'nextButton' : {className: "myNext", text: "Weiter"}
      }
    ];
    enjoyhint_instance.set(enjoyhint_script_steps);
    enjoyhint_instance.run();
  });

  $(document).on("click", ".enjoyhinthelpteams2", function() {
    var enjoyhint_instance = new EnjoyHint({});
    var enjoyhint_script_steps = [
      {
        'click .enjoyhinthelpteamsaddone' : 'Klicke auf "Add", um ein vergessenes Teammitglied hinzuzufügen',
        'skipButton' : {className: "mySkip", text: "Abbrechen"},
        'nextButton' : {className: "myNext", text: "Weiter"}
      }
    ];
    enjoyhint_instance.set(enjoyhint_script_steps);
    enjoyhint_instance.run();
  });

  $(document).on("click", ".enjoyhinthelpteams3", function() {
    var enjoyhint_instance = new EnjoyHint({});
    var enjoyhint_script_steps = [
      {
        'click .enjoyhinthelpteamsback' : 'Klick das "Icon", um zur Firmenübersicht zurück zu gelangen' ,
        'skipButton' : {className: "mySkip", text: "Abbrechen"},
        'nextButton' : {className: "myNext", text: "Weiter"}
      }
    ];
    enjoyhint_instance.set(enjoyhint_script_steps);
    enjoyhint_instance.run();
  });
  

});


// function download() {
//   var download = document.getElementById("download");
//   var image = document.getElementById("myCanvas").toDataURL("image/png").replace("image/png", "image/octet-stream");
//   download.setAttribute("href", image);
//   //download.setAttribute("download","archive.png");
//   }

  

})( jQuery );
