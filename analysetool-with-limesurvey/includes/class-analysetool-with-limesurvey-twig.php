<?php

/**
 * Register all actions and filters for the plugin
 *
 * @link       -
 * @since      1.0.0
 *
 * @package    Analysetool_With_Limesurvey
 * @subpackage Analysetool_With_Limesurvey/includes
 */

/**
 * Register all actions and filters for the plugin.
 *
 * Maintain a list of all hooks that are registered throughout
 * the plugin, and register them with the WordPress API. Call the
 * run function to execute the list of actions and filters.
 *
 * @package    Analysetool_With_Limesurvey
 * @subpackage Analysetool_With_Limesurvey/includes
 * @author     Markus Domin, Helge Nissen <->
 */
class Analysetool_With_Limesurvey_Twig {

	public $twig;
	protected static $_instance = null;

	public static function getInstance()
   {
       if (null === self::$_instance)
       {
           self::$_instance = new self;
       }
       return self::$_instance;
   }

   /**
    * clone
    *
    * Kopieren der Instanz von aussen ebenfalls verbieten
    */
   protected function __clone() {}

	/**
	 * Initialize the collections used to maintain the actions and filters.
	 *
	 * @since    1.0.0
	 */
	protected function __construct() {
		
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/module/Twig/Autoloader.php';

		Twig_AutoLoader::register();

// $included_files = get_included_files();

// foreach ($included_files as $filename) {
//     echo "$filename\n";
// }

		// Specify our Twig templates location
		$loader = new Twig_Loader_Filesystem(plugin_dir_path( dirname( __FILE__ ) ).'templates/v1');

		 // Instantiate our Twig
		$this->twig = new Twig_Environment($loader);

	}

}
