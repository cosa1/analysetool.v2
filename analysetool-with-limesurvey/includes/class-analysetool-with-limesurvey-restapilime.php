<?php

/**
 * Register all actions and filters for the plugin
 *
 * @link       -
 * @since      1.0.0
 *
 * @package    Analysetool_With_Limesurvey
 * @subpackage Analysetool_With_Limesurvey/includes
 */

/**
 * Databaseconnection for the plugin.
 *
 *
 * @package    Analysetool_With_Limesurvey
 * @subpackage Analysetool_With_Limesurvey/includes
 * @author     Markus Domin <markus.domin@fh-luebeck.de>
 */
class Analysetool_With_Limesurvey_Restapilime {

	private $database;
	/**
	 * Initialize .
	 *
	 * @since    1.0.0
	 */
	public function __construct($database) {
		$this->database = $database;
	}

	//http://localhost/wordpresslime/wordpress/wp-json/anatool/v1/evaluationend

function anatool_restapi_evaluationend($request) {
  /*
  {
  		"cid":"15ea7f85d5ecac4c0812c1a624c8eafa",
  		"uid":"6c0d9fa9f4af906e6658923fbcc595b51",
  		"querys":["1","2","3"]
  	}

	*/



    if ( isset( $request['data'] ) ) {

				// Strip HTML Tags
				$clear = strip_tags($request['data']);
				// Clean up things like &amp;
				$clear = html_entity_decode($clear);
				// Strip out any url-encoded stuff
				$clear = urldecode($clear);
				$clear = htmlspecialchars_decode($clear,ENT_QUOTES);
				$clear =str_replace("&quot;","\"",$clear);
				$request['data']=$clear;

        $request['data']=json_decode($request['data'],true);
				echo "save0";

        if ( isset( $request['data']['cid']) &&isset( $request['data']['uid']) && isset( $request['data']['querys'] ) ) {
          echo "save1";

          $userid=$this->database->anatool_db_check_cid_uid(urlencode($request['data']['cid']),urlencode($request['data']['uid']));
          //var_dump($userid);
          if( $userid != null && is_array($request['data']['querys'])){
            echo "save2";
            if($this->database->anatool_db_save_evaluation($userid,$request['data']['querys'])){

              $pageid = get_option( 'anatool_option_name')['evaluationdetail'];
              //umleiten zur abschlusseite
							//header("Location:".get_permalink($pageid),true,301);
							
							header("Location:".get_home_url(),true,301);
              die();
            }
          }

        }
        //return rest_ensure_response( $request['data']);
    }
    //return rest_ensure_response( 'Hello World, this is the WordPress REST API' );

    $response = new WP_REST_Response();
    $response->set_status( 404 );
    $response->header( 'Location', 'http://google.de/' );
    return $response;
	}

	function testget(){
	  header("Location:".get_permalink(2),true,301);
	  /*$response = new WP_REST_Response();
	  $response->set_status( 301 );
	  $response->header( 'Location', ''.get_permalink(2) );
	  return $response;*/
	  die();
	}

	function anatool_restapi_ini() {
	  register_rest_route( 'anatool/v1', '/evaluationend',
	    /*array(
	      // By using this constant we ensure that when the WP_REST_Server changes our readable endpoints will work as intended.
	      'methods'  => WP_REST_Server::READABLE,
	      'callback' => 'testget',
	    ),*/
	    array(
	      'methods'  => WP_REST_Server::CREATABLE,
	      'callback' => array($this, 'anatool_restapi_evaluationend')//'anatool_restapi_evaluationend'
	    )
	  );
	}


}
