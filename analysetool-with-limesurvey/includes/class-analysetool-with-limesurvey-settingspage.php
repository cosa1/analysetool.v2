<?php

/**
 * Register all actions and filters for the plugin
 *
 * @link       -
 * @since      1.0.0
 *
 * @package    Analysetool_With_Limesurvey
 * @subpackage Analysetool_With_Limesurvey/includes
 */

/**
 * Databaseconnection for the plugin.
 *
 *
 * @package    Analysetool_With_Limesurvey
 * @subpackage Analysetool_With_Limesurvey/includes
 * @author     Markus Domin <markus.domin@fh-luebeck.de>
 */
class Analysetool_With_Limesurvey_Settingspage {

	/**
   * Holds the values to be used in the fields callbacks
   */
  private $options;

  private $title = "Analysetool einstellungen";
  private $subtitle = "Einstellung für Limsurvey";
  private $setting_admin_label = "anatool-setting-admin";
  private $option_name = "anatool_option_name";
  private $option_group = "anatool_option_group";
	/**
	 * Initialize .
	 *
	 * @since    1.0.0
	 */
	public function __construct() {
		// add_action( 'admin_menu', array( $this, 'add_plugin_page' ) );
    // add_action( 'admin_init', array( $this, 'page_init' ) );
	}
	/**
   * Add options page
   */
  public function add_plugin_page()
  {
      // This page will be under "Settings"
      add_options_page(
          'Settings Admin',
          ''.$this->title,
          'manage_options',
          ''.$this->setting_admin_label,
          array( $this, 'create_admin_page' )
      );
  }

  /**
   * Options page callback
   */
  public function create_admin_page()
  {
      // Set class property
      $this->options = get_option( ''.$this->option_name );
      ?>
      <div class="wrap">
          <h1><?php echo $this->title;?></h1>
          <form method="post" action="options.php">
          <?php
              // This prints out all hidden setting fields
              settings_fields( ''.$this->option_group );
              do_settings_sections( ''.$this->setting_admin_label );
              submit_button();
          ?>
          </form>
      </div>
      <?php
  }

  /**
   * Register and add settings
   */
  public function page_init()
  {
      register_setting(
          ''.$this->option_group, // Option group
          ''.$this->option_name, // Option name
          array( $this, 'sanitize' ) // Sanitize
      );

      add_settings_section(
          'setting_section_id', // ID
          ''.$this->subtitle, // Title
          array( $this, 'print_section_info' ), // Callback
          ''.$this->setting_admin_label // Page
      );

      add_settings_field(
          'id_number', // ID
          'ID Number', // Title
          array( $this, 'id_number_callback' ), // Callback
          ''.$this->setting_admin_label, // Page
          'setting_section_id' // Section
      );

      add_settings_field(
          'title',
          'Title',
          array( $this, 'title_callback' ),
          ''.$this->setting_admin_label,
          'setting_section_id'
      );

      add_settings_field(
          'limeurl',
          'Limesurvey Url',
          array( $this, 'limeurl_callback' ),
          ''.$this->setting_admin_label,
          'setting_section_id'
      );

      add_settings_field(
          'limeid',
          'Limesurvey Umfragenid',
          array( $this, 'limeid_callback' ),
          ''.$this->setting_admin_label,
          'setting_section_id'
      );

    //   add_settings_field(
    //     'limemultiid',
    //     'Test     test  Limesurvey Umfragenid mehrfach ids(nur eine pro zeile, id;Title )',
    //     array( $this, 'limemultiid_callback' ),
    //     ''.$this->setting_admin_label,
    //     'setting_section_id'
    // );

      add_settings_field(
          'evaluationdetail',
          'Betrieb Dateilauswerte Seite',
          array( $this, 'evaluationdetail_callback' ),
          ''.$this->setting_admin_label,
          'setting_section_id'
      );
      add_settings_field(
          'minical',
          'Mindestmenge an Person die eine Umfrage beendet habe bevor eine Auswertung möglich ist',
          array( $this, 'minical_callback' ),
          ''.$this->setting_admin_label,
          'setting_section_id'
      );
      add_settings_field(
          'companytokenlist',
          'Seite für die Linkübersicht für die Betriebe',
          array( $this, 'companytokenlist_callback' ),
          ''.$this->setting_admin_label,
          'setting_section_id'
      );

      add_settings_field(
        'mailtext',
        'Text für die Mail die an die Betriebsinhaber geht (Am Ende wird der Link Automatisch eingefügt)',
        array( $this, 'mailtext_callback' ),
        ''.$this->setting_admin_label,
        'setting_section_id'
    );

    add_settings_field(
        'mailtext_user',
        'Text für die Mail die an die Mitarbeiter geht (Am Ende wird der Link Automatisch eingefügt)',
        array( $this, 'mailtext_user_callback' ),
        ''.$this->setting_admin_label,
        'setting_section_id'
    );

    add_settings_field(
        'info_shortcode',
        'shortcodes die zu verfügung stehen',
        array( $this, 'info_shortcode_callback' ),
        ''.$this->setting_admin_label,
        'setting_section_id'
    );
  }

  /**
   * Sanitize each setting field as needed
   *
   * @param array $input Contains all settings fields as array keys
   */
  public function sanitize( $input )
  {
      $new_input = array();
      if( isset( $input['id_number'] ) )
          $new_input['id_number'] = absint( $input['id_number'] );

      if( isset( $input['title'] ) )
          $new_input['title'] = sanitize_text_field( $input['title'] );

      $name = "limeurl";
      if( isset( $input[''.$name] ) )
          $new_input[''.$name] = sanitize_text_field( $input[''.$name] );

      $name = "limeid";
      if( isset( $input[''.$name] ) )
          $new_input[''.$name] = sanitize_text_field( $input[''.$name] );

        $name = "limemultiid";
        if( isset( $input[''.$name] ) )
            $new_input[''.$name] = sanitize_textarea_field( $input[''.$name] );
  
      $name = "evaluationdetail";
      if( isset( $input[''.$name] ) )
          $new_input[''.$name] = sanitize_text_field( $input[''.$name] );

      $name = "minical";
      if( isset( $input[''.$name] ) )
          $new_input[''.$name] = absint( $input[''.$name] );

      $name = "companytokenlist";
      if( isset( $input[''.$name] ) )
          $new_input[''.$name] = absint( $input[''.$name] );

      $name = "mailtext";
      if( isset( $input[''.$name] ) )
          $new_input[''.$name] = sanitize_textarea_field( $input[''.$name] );

        $name = "mailtext_user";
        if( isset( $input[''.$name] ) )
            $new_input[''.$name] = sanitize_textarea_field( $input[''.$name] );

      return $new_input;
  }

  /**
   * Print the Section text
   */
  public function print_section_info()
  {
      //print 'Parameter einstellen für Limsurvey:';
  }

  /**
   * Get the settings option array and print one of its values
   */
  public function id_number_callback()
  {
      printf(
          '<input type="text" id="id_number" name="'.$this->option_name.'[id_number]" value="%s" />',
          isset( $this->options['id_number'] ) ? esc_attr( $this->options['id_number']) : ''
      );
  }

  /**
   * Get the settings option array and print one of its values
   */
  public function title_callback()
  {
      printf(
          '<input type="text" id="title" name="'.$this->option_name.'[title]" value="%s" />',
          isset( $this->options['title'] ) ? esc_attr( $this->options['title']) : ''
      );
  }

  public function limeurl_callback()
  {
      $name = "limeurl";
      printf(
          '<input type="text" id="'.$name.'" name="'.$this->option_name.'['.$name.']" value="%s" />',
          isset( $this->options[''.$name] ) ? esc_attr( $this->options[''.$name]) : ''
      );
  }

  public function limeid_callback()
  {
      $name = "limeid";
      printf(
          '<input type="text" id="'.$name.'" name="'.$this->option_name.'['.$name.']" value="%s" />',
          isset( $this->options[''.$name] ) ? esc_attr( $this->options[''.$name]) : ''
      );
  }

  public function limemultiid_callback()
  {
      $name = "limemultiid";
      printf(
          //'<input type="text" id="'.$name.'" name="'.$this->option_name.'['.$name.']" value="%s" />',
          '<textarea id="'.$name.'" name="'.$this->option_name.'['.$name.']" rows="10" cols="80">%s</textarea>',
          isset( $this->options[''.$name] ) ? esc_attr( $this->options[''.$name]) : ''
      );
  }

  public function evaluationdetail_callback()
  {
      $name = "evaluationdetail";
      printf(
          '<input type="text" id="'.$name.'" name="'.$this->option_name.'['.$name.']" value="%s" />',
          isset( $this->options[''.$name] ) ? esc_attr( $this->options[''.$name]) : ''
      );
  }

  public function minical_callback()
  {
      $name = "minical";
      printf(
          '<input type="number" id="'.$name.'" name="'.$this->option_name.'['.$name.']" value="%s" />',
          isset( $this->options[''.$name] ) ? esc_attr( $this->options[''.$name]) : ''
      );
  }

  public function companytokenlist_callback()
  {
      $name = "companytokenlist";
      printf(
          '<input type="number" id="'.$name.'" name="'.$this->option_name.'['.$name.']" value="%s" />',
          isset( $this->options[''.$name] ) ? esc_attr( $this->options[''.$name]) : ''
      );
  }

  public function mailtext_callback()
  {
      $name = "mailtext";
      printf(
          //'<input type="text" id="'.$name.'" name="'.$this->option_name.'['.$name.']" value="%s" />',
          '<textarea id="'.$name.'" name="'.$this->option_name.'['.$name.']" rows="10" cols="80">%s</textarea>',
          isset( $this->options[''.$name] ) ? esc_attr( $this->options[''.$name]) : ''
      );
  }

  public function mailtext_user_callback()
  {
      $name = "mailtext_user";
      printf(
          //'<input type="text" id="'.$name.'" name="'.$this->option_name.'['.$name.']" value="%s" />',
          '<textarea id="'.$name.'" name="'.$this->option_name.'['.$name.']" rows="10" cols="80">%s</textarea>',
          isset( $this->options[''.$name] ) ? esc_attr( $this->options[''.$name]) : ''
      );
  }

  public function info_shortcode_callback()
  {
      $name = "info_shortcode";
     ?>
     <div id="infoshortcodes">
    <h2 style="border-bottom: 1px solid #000;
    width: 100%;
    display: block;">Diagramm erzeugen</h2>
        <br>
        <table>
            <tr>
                <th>shortcode</th>
                <th>Ausgabe</th>
            </tr>
            <tr>
                <td>[anatool_groups_detail_chart labels=berforderung;Zeitdruck;Fehlende Arbeitsmittel;Kompetenzmangel im Team;Unklare Zust&auml;ndigkeit;Zeitdruck im Team;Fluktuation und Unterbesetzung;Schwierige Situationen mit Kunden questions=40,41,42;37,38,39;58,59;80,81,82;83,84;85;99,100,101,102;43,44,45,46,47 title=Stress selectgroups=1 plots=2.5;3.5 colorarea=0:g;0.5:y;1:r labelsx =leer;sehr wenig;wenig;mittel;viel;sehr viel]</td>
                <td>(Grafik)</td>
            </tr>
        </table> 
        <br> 
        <table>
            <tbody>
                <tr>
                    <th >Parameter</th>
                    <th >Wert</th>
                    <th >Bedeutung</th>
                </tr>
                <tr>
                    <td >Labels</td>
                    <td >
                        berforderung;Zeitdruck;Fehlende Arbeitsmittel;Kompetenzmangel im Team;Unklare Zust&auml;ndigkeit;Zeitdruck im Team;Fluktuation und Unterbesetzung;Schwierige Situationen mit Kunden<br></td>
                    <td >Achesenbeschriftung&nbsp;</td>
                </tr>
                <tr>
                    <td >
                        questions<br></td>
                    <td >
                        40,41,42;37,38,39; 58,59;80,81,82; 83,84;85; 99,100,101,102;43,44,45,46,47<br></td>
                    <td >
                        <ul>
                            <li>enth&auml;lt die einzelnen Scalen</li>
                            <li>Scale1,Scale2, Scale3 (mit kommegetrent werden zusammen gerechnet zu einem Wert.</li>
                            <li>Einzelnen Abschnitte werden mit Semikolon getrent</li>
                            <li>Wichtig: ohne Leerzeichen eingeben</li>
                        </ul>
                    </td>
                </tr>
                <tr>
                    <td >title</td>
                    <td >Stress</td>
                    <td >Diagramm Titel</td>
                </tr>
                <tr>
                    <td >
                        plots<br></td>
                    <td >
                        2.5;3.5<br></td>
                    <td >optional<br>vertikale gestrichelte linie</td>
                </tr>
                <tr>
                    <td >
                        colorarea<br></td>
                    <td >
                        0:g;0.5:y;1:r <br></td>
                    <td >optional<br>farbverlauf<br>0=links, 1 ganz rechts<br>g=grn,y=gelb;r=rot</td>
                </tr>
                <tr>
                    <td >
                        labelsx<br></td>
                    <td >
                        leer;sehr wenig;wenig;mittel;viel;sehr viel<br></td>
                    <td >optional<br>beschriftung der Achse von 0 beginnent</td>
                </tr>
            </tbody>
        </table>
            
        <h2 style="border-bottom: 1px solid #000;
    width: 100%;
    display: block;">variable ausgeben</h2>
        <br>
        <table>
            <tr>
                <th>shortcode</th>
                <th>Ausgabe</th>
            </tr>
            <tr>
                <td>[anatool_groups_detail_value questions="40,41,42"]</td>
                <td>42.5</td>
            </tr>
        </table> 
        <br>
        <table>
            <tr>
                <th>parameter</th>
                <th>Wert</th>
                <th>Bedeutung</th>
            </tr>
            <tr>
                <td>questions</td>
                <td>40,41,42</td>
                <td>
                    optional<br>
                    liste mit den einzelnen scalen komma getrent<br>
                    (das minus gibt an das die scala invertiert ist)
                </td>
            </tr>
        </table>
        
        

        
        <h2 style="border-bottom: 1px solid #000;
    width: 100%;
    display: block;">Abfrage</h2>
        <br>
        <table>
            <tr>
                <th>shortcode</th>
                <th>Ausgabe</th>
            </tr>
            <tr>
                <td>[anatool_groups_detail_ifblock questions="37,38,-39" max=2.5 operator="<"]  belibiger text  [/anatool_groups_detail_ifblock]</td>
                <td>belibiger text (aber nur wenn die abfrage wahr ist)</td>
            </tr>
        </table> 
        <br>
        <table>
            <tr>
                <th>parameter</th>
                <th>Wert</th>
                <th>Bedeutung</th>
            </tr>
            <tr>
                <td>questions</td>
                <td>37,38,-39</td>
                <td>
                    optional<br>
                    liste mit den einzelnen scalen komma getrent<br>
                    (das minus gibt an das die scala invertiert ist)
                </td>
            </tr>
            <tr>
                <td>max</td>
                <td>2.5</td>
                <td>die zahl die den maximalwrt für die abfrage enthält</td>
            </tr>
            <tr>
                <td>operator</td>
                <td>">"</td>
                <td>der vergleichsoperator für die abfrage</td>
            </tr>
        </table>
        

        <h2 style="border-bottom: 1px solid #000;
    width: 100%;
    display: block;">verschahtelte innere Abfrage kann innerhalb der anatool_groups_detail_ifblock verwendet werden</h2>
        <br>
        <table>
            <tr>
                <th>shortcode</th>
                <th>Ausgabe</th>
            </tr>
            <tr>
                <td>[anatool_groups_detail_inner_ifblock questions="37,38,-39" max=2.5 operator="<"]  belibiger text  [/anatool_groups_detail_inner_ifblock]</td>
                <td>belibiger text (aber nur wenn die abfrage wahr ist)</td>
            </tr>
        </table> 
        <br>
        <table>
            <tr>
                <th>parameter</th>
                <th>Wert</th>
                <th>Bedeutung</th>
            </tr>
            <tr>
                <td>questions</td>
                <td>37,38,-39</td>
                <td>
                    optional<br>
                    liste mit den einzelnen scalen komma getrent<br>
                    (das minus gibt an das die scala invertiert ist)
                </td>
            </tr>
            <tr>
                <td>max</td>
                <td>2.5</td>
                <td>die zahl die den maximalwrt für die abfrage enthält</td>
            </tr>
            <tr>
                <td>operator</td>
                <td>">"</td>
                <td>der vergleichsoperator für die abfrage</td>
            </tr>
        </table>           
            
        </div>
    
    <?php
  }
  

}
