<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       -
 * @since      1.0.0
 *
 * @package    Analysetool_With_Limesurvey
 * @subpackage Analysetool_With_Limesurvey/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Analysetool_With_Limesurvey
 * @subpackage Analysetool_With_Limesurvey/includes
 * @author     Markus Domin <markus.domin@fh-luebeck.de>
 */
class Analysetool_With_Limesurvey {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      Analysetool_With_Limesurvey_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $plugin_name    The string used to uniquely identify this plugin.
	 */
	protected $plugin_name;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

	protected $settingspage;

	protected $restapilime;

	protected $database;


	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {

		$this->plugin_name = 'analysetool-with-limesurvey';
		$this->version = '1.0.0';

		$this->load_dependencies();
		$this->set_locale();
		$this->define_admin_hooks();
		$this->define_public_hooks();

		ini_set('display_errors', 1);
		ini_set('display_startup_errors', 1);
		error_reporting(E_ALL);


	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - Analysetool_With_Limesurvey_Loader. Orchestrates the hooks of the plugin.
	 * - Analysetool_With_Limesurvey_i18n. Defines internationalization functionality.
	 * - Analysetool_With_Limesurvey_Admin. Defines all hooks for the admin area.
	 * - Analysetool_With_Limesurvey_Public. Defines all hooks for the public side of the site.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {

		/**
		 * The class for rest api für lime survey interaction.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-analysetool-with-limesurvey-twig.php';

		/**
		 * The class for rest api für lime survey interaction.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-analysetool-with-limesurvey-restapilime.php';

		/**
		 * The class for settingspage interaction.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-analysetool-with-limesurvey-settingspage.php';

		/**
		 * The class for Database interaction.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-analysetool-with-limesurvey-database.php';
		//require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-analysetool-with-limesurvey-database-multiquiz.php';

		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-analysetool-with-limesurvey-loader.php';

		/**
		 * The class responsible for defining internationalization functionality
		 * of the plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-analysetool-with-limesurvey-i18n.php';

		/**
		 * The class responsible for defining all actions that occur in the admin area.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-analysetool-with-limesurvey-admin.php';

		/**
		 * The class responsible for defining all actions that occur in the public-facing
		 * side of the site.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-analysetool-with-limesurvey-public.php';

		$this->loader = new Analysetool_With_Limesurvey_Loader();
		$this->settingspage = new Analysetool_With_Limesurvey_Settingspage();
		$this->database = new Analysetool_With_Limesurvey_Database();
		$this->restapilime = new Analysetool_With_Limesurvey_Restapilime($this->database);


	}

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the Analysetool_With_Limesurvey_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function set_locale() {

		$plugin_i18n = new Analysetool_With_Limesurvey_i18n();

		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );

	}

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_admin_hooks() {

		$this->loader->add_action( 'admin_menu', $this->settingspage, 'add_plugin_page');
    $this->loader->add_action( 'admin_init', $this->settingspage, 'page_init' );

		$this->loader->add_action( 'rest_api_init', $this->restapilime, 'anatool_restapi_ini' );

		$plugin_admin = new Analysetool_With_Limesurvey_Admin( $this->get_plugin_name(), $this->get_version(),$this->database );

		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_styles' );
		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts' );

	}

	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_public_hooks() {

		$plugin_public = new Analysetool_With_Limesurvey_Public( $this->get_plugin_name(), $this->get_version() ,$this->database);

		$this->loader->add_filter( 'style_loader_tag', $plugin_public, 'make_stylesheet_less' );

		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_styles' );
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_scripts' );

		$this->loader->add_shortcode( 'anatool_create_company', $plugin_public, 'anatool_frontend_create_company' );
		$this->loader->add_shortcode( 'anatool_show_companys', $plugin_public, 'anatool_frontend_show_companys' );
		$this->loader->add_shortcode( 'anatool_show_select_companys', $plugin_public, 'anatool_frontend_show_select_companys' );
		$this->loader->add_shortcode( 'anatool_create_groups', $plugin_public, 'anatool_frontend_create_groups' );
		$this->loader->add_shortcode( 'anatool_show_groups', $plugin_public, 'anatool_frontend_output_groups' );
		$this->loader->add_shortcode( 'anatool_show_token_company', $plugin_public, 'anatool_frontend_show_token_company' );


		//auswerte seite

        $this->loader->add_shortcode( 'anatool_groups_Member', $plugin_public, 'showCompanyInfo' );
        $this->loader->add_shortcode( 'anatool_groups_detail_chart', $plugin_public, 'anatool_frontend_chart' );
		$this->loader->add_shortcode( 'anatool_groups_detail_ifblock', $plugin_public, 'anatool_frontend_ifblock');
		$this->loader->add_shortcode( 'anatool_groups_detail_inner_ifblock', $plugin_public, 'anatool_frontend_inner_ifblock');
		$this->loader->add_shortcode( 'anatool_groups_detail_value', $plugin_public, 'anatool_frontend_value');
		//ende auswerte seite

		$this->loader->add_shortcode( 'anatool_groups_benchdata', $plugin_public, 'insertbenchmark');
		$this->loader->add_shortcode( 'anatool_groups_benchdata_remove', $plugin_public, 'removebenchmark');

		$analysetool_With_Limesurvey_Restapilime = new Analysetool_With_Limesurvey_Restapilime($this->database);
		$this->loader->add_action( 'rest_api_init', $analysetool_With_Limesurvey_Restapilime, 'anatool_restapi_ini' );

//v2
		$this->loader->add_shortcode( 'analysetool_dashboard', $plugin_public, 'analysetool_dashboard' );
		$this->loader->add_shortcode( 'analysetool_linklist', $plugin_public, 'analysetool_linklist' );



	}

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_plugin_name() {
		return $this->plugin_name;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    Analysetool_With_Limesurvey_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}

}
