<?php

/**
 * Fired during plugin deactivation
 *
 * @link       -
 * @since      1.0.0
 *
 * @package    Analysetool_With_Limesurvey
 * @subpackage Analysetool_With_Limesurvey/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Analysetool_With_Limesurvey
 * @subpackage Analysetool_With_Limesurvey/includes
 * @author     Markus Domin <markus.domin@fh-luebeck.de>
 */
class Analysetool_With_Limesurvey_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
